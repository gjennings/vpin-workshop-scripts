'***************************************************************
' ZDAT: VPIN WORKSHOP DATA MANAGER - 1.1.0 BETA
'***************************************************************
' WHAT IS IT?
' The VPin Workshop Data Manager is a tool to manage key/value
' data and sync it with the VPReg. This allows you to set a
' standard data structure, modify it during games and whatnot,
' save it to the VPReg, and automatically load the saved value
' when you next load the game and initialize the class.
'
' This class may be useful for managing data such as high
' scores, settings from operator menus, game play progress
' (especially if you want to support saving / resuming games),
' and table / game statistics.
'
' NOTE: This library might not work on 32-bit set-ups! This is
' because data loaded into the Dictionary from the VPReg is
' converted to type Double if it is numeric.
'
' HOW TO USE
' 1) Put this VBS file in your VPX Scripts folder, or
'    copy / paste the code into your table script
'    (and skip step 2).
' 2) Include this VBS file in your table via ExecuteGlobal and
'    Scripting.FileSystemObject.
' 3) Construct one or more classes (example:)
'    Dim highScores
'    Set highScores = New vpwData
'    With highScores
'         .gameName = "tableName"
'         .dataName = "nameOfDataset"
'         .Load "keyName", "defaultValue"
'         .Load "anotherKey", 0
'         .Reset = true
'    End With
' 4) For each vpwData, call SaveAll in the table_exit
'    event:
'    highScores.SaveAll
' 5) Refer to the methods below for more info.
'---------------------------------------------------------------
' TIPS FOR CREATING / USING DATA CLASSES
' 1) Use vpwData.data.Item(keyName) to get the value of a
'    key. Use vpwData.data.Item(keyName) = newValue to set
'    a new value for a key, and vpwData.Save keyName if
'    you want to save the new value via SaveValue immediately.
'    ONLY use strings or numbers as values!
' 2) When tracking player-specific data, such as game progress,
'    you may want to use a For loop to create a dataset for
'    each player (ex. if your table can support up to 4 players,
'    create 4 datasets):
'    Dim dataPlayer(4)
'    Dim i
'    For i = 0 to 3
'        Set dataPlayer(i) = New vpwData
'        With dataPlayer(i)
'             .gameName = "thisTable"
'             .dataName = "player" & i
'            .Load "score", 0
'            .Load "modesCompleted", 0
'        End With
'    Next
' 3) WARNING: the total character length of each key may not
'    exceed (31 - character length of pDataName in Init call)
'    characters. This is a VPReg key length limitation.
' 4) WARNING: Due to VPReg limitations, only strings and
'    numbers are supported for values. Do not use booleans
'    (use 0 or 1 instead), arrays, objects, etc.
' 5) Avoid repeatedly saving data keys that frequently change;
'    you should only save for significant changes (credits
'    inserted, extra balls, etc) and when exiting the table.
'    You can also SaveAll during downtime in the game, such as
'    when the ball is held in a saucer while something is
'    playing on the DMD, or a change in player turns / balls.
'
' TUTORIAL: https://youtu.be/STAc5ykyWl4
'***************************************************************

Class vpwData
	Public data ' Scripting.Dictionary of the current key/value data
	Public gameName ' Alphanumeric name of this table
	Public dataName ' Name of this dataset, alphanumeric; MUST be set when initializing the class and MUST be unique for each dataset.
	
	Private defaults ' Dictionary of data.Keys to their default values
	Private dataReg	 ' Dictionary of data.Keys to the value loaded from the VP Registry (for tracking which ones to save in SaveAll)
	
	'----------------------------------------------------------
	' Class_Initialize
	' Class initializer
	'----------------------------------------------------------
	Public Sub Class_Initialize
		Set data = CreateObject("Scripting.Dictionary")
		Set defaults = CreateObject("Scripting.Dictionary")
		Set dataReg = CreateObject("Scripting.Dictionary")
	End Sub
	
	'----------------------------------------------------------
	' vpwData.Load
	' Load a data key into the data manager.
	'
	' You MUST set gameName and dataName first!
	'
	' PARAMETERS
	' key (String) - The name of the key to load
	' defaultValue (String|Number) - The default value to use
	' for this key when reset or when the value does not yet
	' exist in VPReg.
	'
	' NOTE: The data is loaded immediately after call into
	' vpwData.data with either the value that was saved
	' in VPReg or the specified default value. Once Save or
	' SaveAll is called, whatever value is set for this key in
	' vpwData.data will be saved to VPReg (even if it is
	' the default value! This means you will need to set the
	' value of the key to an empty string if you want to reset
	' it to the defined default value [such as if you change
	' the default] or set .reset = true on the dataset).
	'----------------------------------------------------------
	Public Sub Load(key, defaultValue)
		' Note the default value requested
		defaults.Add key, defaultValue
		
		' Try loading the saved value from VPReg
		Dim savedValue
		savedValue = LoadValue(gameName, dataName & "." & key)
		If savedValue = "" Then ' Blank / no value; use default
			data.Add key, defaultValue
			dataReg.Add key, ""
		Else
			If IsNumeric(savedValue) Then ' Convert numerical values to double
				data.Add key, CDbl(savedValue)
				dataReg.Add key, CDbl(savedValue)
			Else ' Convert non-numerical values to string
				data.Add key, CStr(savedValue)
				dataReg.Add key, CStr(savedValue)
			End If
		End If
	End Sub
	
	'----------------------------------------------------------
	' vpwData.Default
	' Get the default value of a key.
	'
	' PARAMETERS
	' Key (string) - The key of which to get the default value
	'----------------------------------------------------------
	Public Property Get Default(Key)
		Default = defaults.Item(Key)
	End Property
	
	'----------------------------------------------------------
	' vpwData.Save
	' Save a key's current value to the VPX registry via
	' SaveValue, even if we believe it has not changed.
	'
	' PARAMETERS
	' pKey (string) - The name of the key to save
	'----------------------------------------------------------
	Public Sub Save(pKey)
		Dim i
		If data.Exists(pKey) Then
			i = data.Item(pKey)
			SaveValue gameName, dataName & "." & pKey, i
			dataReg.Item(pKey) = i
		End If
	End Sub
	
	'----------------------------------------------------------
	' vpwData.SaveAll
	' Save the values of all keys which we believe have changed
	' to the VP registry (via SaveValue).
	'----------------------------------------------------------
	Public Sub SaveAll()
		Dim i
		Dim i2
		For Each i In data.Keys
			i2 = data.Item(i)
			
			'Optimize by only saving keys which we think have a different value
			If Not dataReg.Item(i) = i2 Then
				SaveValue gameName, dataName & "." & i, i2
				dataReg.Item(i) = i2
			End If
		Next
	End Sub
	
	'----------------------------------------------------------
	' vpwData.Reset = True|False
	' Reset all of the key's values back to their defaults and
	' save (if True). Should not be called until all keys have
	' been loaded via the Load routine.
	'----------------------------------------------------------
	Public Property Let Reset(doIt)
		If doIt = True Then
			Dim key
			For Each key In defaults.Keys
				data.Item(key) = defaults.Item(key)
			Next
		End If
	End Property
End Class

'**************************************************************
'   END VPW DATA MANAGER
'**************************************************************