# VPin Workshop Scripts

Here is a repository of useful VBS scripts / libraries others can use in their Visual Pinball X tables. Please see the relevant script for more information on what it does.

## Version Tags
 - Alpha: The script does not have all the planned features implemented yet. Bugs are highly likely.
 - Beta: While all planned features were implemented, the script has not yet been thoroughly tested. Bugs are likely.
 - (No tag): The script is considered stable and bugs are not likely. New features are still welcomed.
 - Final: No new features are being accepted for this script; bugs will still be fixed when reported.

## Installation
Simply copy / paste the VBS code into your table script, or save the VBS file in your Scripts directory and execute it globally via this method:
```
    On Error Resume Next
    ExecuteGlobal GetTextFile("nameOfFile.vbs")
    If Err Then MsgBox "You need the nameOfFile.vbs file in your Scripts folder in order To run this table."
    On Error GoTo 0
```

## Usage
Each script has its own usage instructions. Please carefully read the comments in the script you desire to use.

## Support
If you encounter any bugs or issues with something, please open an Issue on GitLab. Please thoroughly describe the issue and what you are trying to do (including relevant code).

## Roadmap
It is never clear to me what will be developed next. I tend to develop based on my own needs and the needs of others as I see / identify them and as I have the time to do so. As for these scripts, they will continue to be maintained with bug fixes and, so long as the version does not end with "FINAL", new features as requested.

## Contributing
If you would like to make your own contributions or fixes to these scripts, please clone this repository, make your changes, and then create a pull request. I would be happy to include them if your changes fix a bug or provide a new useful feature.

## Authors and acknowledgment
The following people have contributed to one or more of these scripts:
 - Arelyel Krele (Patrick Schmalstig): Original author of all the scripts

## License
These scripts are licensed under the same license as [Visual Pinball X](https://github.com/vpinball/vpinball/blob/master/LICENSE).

## Project status
Active