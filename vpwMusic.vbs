'***************************************************************
' ZMUS: VPIN WORKSHOP MUSIC - 0.2.3 ALPHA
'***************************************************************
' WHAT IS IT?
' The VPin Workshop Music class is a small wrapper around the
' VPin Workshop Tracks class. It allows managing a group of
' sound tracks as music tracks with automatic fading and
' stopping of previous music tracks when starting a new one.
'
' REQUIRES vpwTracks and vpwTrack
'
' HOW TO USE
' 1) Add vpwTracks and vpwTrack scripts to your table. You do
'    not need to follow set-up instructions beyond including 
'    the script unless you will be using fading on individual
'    non-music sounds as well.
' 2) Put this VBS file in your VPX Scripts folder, or
'    copy / paste the code into your table script
'    (and skip step 3).
' 3) Include this VBS file in your table via ExecuteGlobal and
'    Scripting.FileSystemObject.
' 4) Initialize the class on a variable:
'    	Dim Music : set Music = new vpwMusic
' 5) Use a VPX timer to tick the fade with vpwMusic.Tick.
'    You can use any interval, but the lower the interval, the
'    more smooth the fading. If you already have a timer for
'    vpwTracks, it is highly recommend to just put
'    vpwMusic.Tick in that timer's timer event instead of
'    creating another timer.
' 6) Add your music tracks to the group via vpwMusic.Add.
' 7) Refer to the rest of this class for instructions.
'
' Note: You can initialize multiple classes for multiple
' groups of music, but normally you would not do this as you
' would probably only ever want one music track playing at a
' time.
'
' TUTORIAL: https://youtu.be/C0J1LT5-wqA
'***************************************************************

Class vpwMusic
	Public trackManager		'The vpwTracks class to use for managing the music
	Public nowPlaying 		'The name of the music track currently playing or last played, or null if nothing is playing
	Private data 			'Dictionary of music: key is the name of the sound in VPX, value is An array of default volume, fadeIn, fadeOut.
	
	Private Sub Class_Initialize
		Set trackManager = New vpwTracks
		Set data = CreateObject("Scripting.Dictionary")
		nowPlaying = Null
	End Sub
	
	'----------------------------------------------------------
	' vpwMusic.Add
	' Add a sound to this music manager. This means it will be
	' stopped when any other music track in the same class is
	' started, or when StopTrack is called.
	'
	' PARAMETERS:
	' mName (String) - The name of the sound in VPX's sound
	' manager.
	' loopCount (Integer) - The number of times this music
	' track should play (at maximum). Use -1 for unlimited.
	' volume (float) - The default volume to use (0 - 1).
	' fadeIn (Integer) - The number of milliseconds to fade In
	' this track when starting to play it.
	' fadeOut (Integer) - The number of milliseconds to fade
	' Out this track when stopping it.
	'----------------------------------------------------------
	Public Sub Add(mName, loopCount, volume, fadeIn, fadeOut)
		data.Item(mName) = Array(loopCount, volume, fadeIn, fadeOut)
	End Sub
	
	'----------------------------------------------------------
	' vpwMusic.PlayTrack
	' Begin playing a music track while stopping the currently
	' playing track (if applicable).
	'
	' PARAMETERS:
	' mName (String) - The name of the sound to play.
	' loopCount (Integer) - The number of times this music
	' track should play. Use -1 for unlimited. Null = default.
	' volume (float) - The volume to use (0 - 1). Null = use
	' default.
	' fade (Integer) - The fade duration in milliseconds
	' for this track. Null = use default.
	' fadeOut (Integer) - The fade out duration in milliseconds
	' for the currently playing track. Null = use default.
	' Pan (float) - ranges from -1.0 (left) over 0.0 (both) to
	' 1.0 (right).
	' randomPitch (float) - ranges from 0.0 (no randomization)
	' to 1.0 (vary between half speed to double speed).
	' pitch (integer) - can be positive or negative and directly
	' adds onto the standard sample frequency.
	' frontRearFade (float) - similar to pan but fades between
	' the front and rear speakers.
	'----------------------------------------------------------
	Public Sub PlayTrack(mName, loopCount, volume, fade, fadeOut, pan, randomPitch, pitch, frontRearFade)
		If Not data.Exists(mName) Then Exit Sub
		
		' Determine our volumes and fading
		Dim loopCountToUse, volumeToUse, fadeToUse, fadeOutToUse
		If IsNull(loopCount) Then
			loopCountToUse = data.Item(mName)(0)
		Else
			loopCountToUse = loopCount
		End If
		If IsNull(volume) Then
			volumeToUse = data.Item(mName)(1)
		Else
			volumeToUse = volume
		End If
		If IsNull(fade) Then
			fadeToUse = data.Item(mName)(2)
		Else
			fadeToUse = fade
		End If
		
		If Not IsNull(nowPlaying) Then
			'Stop / fade out the current track if it is not the same as the track we are requesting
			If Not mName = nowPlaying Then
				If IsNull(fadeOut) Then
					fadeOutToUse = data.Item(nowPlaying)(3)
				Else
					fadeOutToUse = fadeOut
				End If
				
				If fadeOutToUse = 0 Then ' No fade-out, stop immediately
					trackManager.StopTrack nowPlaying
				Else
					trackManager.AddFade nowPlaying, "volume", 0, fadeOutToUse
				End If
			End If
		End If
		
		nowPlaying = mName
		
		' Start the sound
		If fadeToUse = 0 Then ' No fade-in, start immediately at set volume
			trackManager.PlayTrack mName, loopCountToUse, volumeToUse, pan, randomPitch, pitch, True, True, frontRearFade
		Else
			trackManager.PlayTrack mName, loopCountToUse, 0, pan, randomPitch, pitch, True, True, frontRearFade
			trackManager.AddFade mName, "volume", volumeToUse, fadeToUse
		End If
	End Sub
	
	'----------------------------------------------------------
	' vpwMusic.StopTrack
	' Stop the currently-playing track. Use PlayTrack if you
	' are immediately starting a different track.
	'
	' PARAMETERS:
	' fadeOut (Integer) - The fade out duration in milliseconds
	' for the currently playing track. Null = use default.
	'----------------------------------------------------------
	Public Sub StopTrack(fadeOut)
		If IsNull(nowPlaying) Then Exit Sub ' Nothing to stop
		
		' Get our fade-out duration
		Dim fadeOutToUse
		If IsNull(fadeOut) Then
			fadeOutToUse = data.Item(nowPlaying)(2)
		Else
			fadeOutToUse = fadeOut
		End If
		
		' Stop the sound
		If fadeOutToUse = 0 Then ' No fade-out, stop immediately
			trackManager.StopTrack nowPlaying
		Else
			trackManager.AddFade nowPlaying, "volume", 0, fadeOutToUse
		End If
		
		nowPlaying = Null
	End Sub
	
	Public Sub Tick()
		trackManager.Tick
	End Sub
End Class

'**************************************************************
'   END VPWMUSIC
'**************************************************************