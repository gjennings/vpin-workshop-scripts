'-------------------------------
' Pendulum Nudge v0.0.3-alpha
' by VPin Workshop
'-------------------------------

' CREDITS
' Arelyel - Scripting
' Apophis - Some of the formulas / calculations
' Bord, Flux - Guidance based on IRL tilt bobs

' For use with core.vbs version 3.59 or greater

' This nudge plugin aims to provide a virtual simulation of a tilt 
' bob using math for pendulum motion. This set-up is great for
' those without an actual tilt bob who want realistic tilt mech
' calculations. This solution is still not perfect and does not 
' factor in nuances such as air resistance or damping.

' This script assumes a standard tilt bob set-up:
' * The contact bracket is dead level.
' * The hook and bob point straight down.
' * The pivot point is dead center of the contact bracket.

' To set this up, follow these steps:
'
' 1. Put this file in your Visual Pinball\Scripts directory.
'	 It MUST be named NudgePlugIn.vbs
'
' 2. Set settings using the properties below. Keep
'	 in mind some of the settings are unconventional when
'	 compared to other nudge plugins.

Option Explicit

' cvpmNudge2 (Object = vpmNudge)
'	(Public)  .BobLength	 		- Length of the hook to the tilt bob, in meters
'	(Public)  .BobMass		 		- Weight of the tilt bob pendulum, in kilograms
'	(Public)  .BobSwingDistance		- When the tilt bob is still, the distance in meters the tilt bob may swing in any direction before it hits the contact bracket
'   (Public)  .NewtonsPerForce		- Amount of newtons to apply per 1 force of DoNudge
'	(Public)  .DebugInterval		- Enable output of the current tilt bob values every x milliseconds to the debug log (0 = disable)

'	(Public)  .TiltSwitch	 		- set tilt switch
'	(Public)  .Sensitivity	 		- Set tiltsensitivity (0-10) [TODO: probably use this for the amount of motion a shake applies to the pendulum]
'	(Public)  .NudgeSound	 		- set nudge sound, has to be in table
'	(Public)  .TiltSound	 		- set tilt sound, has to be in table
'	(Public)  .NudgeMusic	 		- mp3 file with nudge sound in music dir
'	(Public)  .TiltMusic	 		- mp3 file with tilt sound in music dir
'	(Public)  .TiltObj		 		- Set objects affected by tilt
'	(Public)  .DoNudge dir,power 	- Nudge table
'	(Public)  .SolGameOn	 		- Game On solenoid handler
'	(Private) .Update		 		- Re-calculate pendulum motion and handle tilting

class cvpmNudge2
	Public TiltSwitch, DebugInterval, NewtonsPerForce
    Private mSlingBump, mForce, mSensitivity, mDebugTime

	' Tilt Bob
	Private mLength, mMass, mDistance
	Private pAngle, pDistance, pTime

	Private Sub Class_Initialize
		TiltSwitch = 0
		mSensitivity = 5
		DebugInterval = 0
		vpmTimer.AddResetObj Me

		' Calculated based on the following knowledge:
		' * We know a pinball ball is 0.0269875 meters and 50 VPX units in diameter. Therefore, 1 VPX unit is 0.00053975 meters.
		' * We know a total change in (Absolute value of velX + Absolute value of velY) of the table will equal the force value specified in the Nudge routine.
		' * We know 1 velX or 1 velY = 100 VPX units per second, therefore 0.053975 meters per second.
		' * Therefore, the table will displace by 0.053975 meters per second per 1 Nudge force.
		' * If we assume the average table is 125 kg in weight, then displacing it by 0.053975 meters per second (1 Nudge force) will require 6.746875 Newtons of force.
		NewtonsPerForce = 6.746875

		pTime = GameTime
		mLength = 1 'TODO: change to the standard tilt bob length in pinball
		mMass = 1 'TODO: change to the standard tilt bob mass in pinball
		mDistance = 1 'TODO: change to the standard distance between the tilt bob and the contact bracket in pinball
	End sub

	Private Property Let NeedUpdate(aEnabled)
		vpmTimer.EnableUpdate Me, False, aEnabled
		If aEnabled Then pTime = GameTime
	End Property

	Public Property Let TiltObj(aSlingBump)
		Dim ii
		ReDim mForce(vpmSetArray(mSlingBump, aSlingBump))
		For ii = 0 To UBound(mForce)
			If TypeName(mSlingBump(ii)) = "Bumper" Then mForce(ii) = mSlingBump(ii).Threshold
			If vpmVPVer >= 90 and TypeName(mSlingBump(ii)) = "Wall" Then mForce(ii) = mSlingBump(ii).SlingshotThreshold
		Next
	End Property

	Public Property Let Sensitivity(aSens)
		mSensitivity = (10-aSens)+1 'TODO
	End property

	Public Property Let BobLength(aLen)
		mLength = aLen
	End Property

	Public Property Let BobMass(aMass)
		mMass = aMass
	End Property

	Public Property Let BobSwingDistance(aDistance)
		mDistance = aDistance
	End Property

	Public Sub DoNudge(ByVal aDir, ByVal aForce)
		' TODO: VPX nudge or vpmTimer.PulseSw TiltSwitch; tilt bob / pendulum shake
		' (TODO: Need to determine the relationship between VPX nudge angle / force and table / tilt mech displacement amount)

		NeedUpdate = True
	End sub

	Public Sub Update
		'Calculate time since last update
		Dim timeDifference
		timeDifference = GameTime - pTime
		pTime = GameTime

		'TODO: is there a more efficient way to determine if a tilt happened in the elapsed time than a loop?
		Do While timeDifference > 0
			'TODO: Calculate the motion of the tilt bob / pendulum
			'TODO: If mDebugTime <= 0 Then Debug.print pendulum stuff : mDebugTime = DebugInterval
			'TODO: If velocity < 0.something And distanceFromCenter < 0.something Then velocity = 0 : distanceFromCenter = 0 : NeedsUpdate = False : Exit Do
			timeDifference = timeDifference - 1
		Loop
	End Sub

	Public Sub Reset
		'TODO
	End Sub

	Public Sub SolGameOn(aEnabled)
		if IsEmpty(mForce) then exit sub 'prevent errors if vpmNudge.TiltObj isn’t set
		Dim obj, ii
		If aEnabled Then
			ii = 0
			For Each obj In mSlingBump
				If TypeName(obj) = "Bumper" Then obj.Threshold = mForce(ii)
				If vpmVPVer >= 90 and TypeName(obj) = "Wall" Then obj.SlingshotThreshold = mForce(ii)
				ii = ii + 1
			Next
		Else
			For Each obj In mSlingBump
				If TypeName(obj) = "Bumper" Then obj.Threshold = 100
				If vpmVPVer >= 90 and TypeName(obj) = "Wall" Then obj.SlingshotThreshold = 100
			Next
		End If
	End Sub
End Class

'-------------------------------
' CHANGELOG
'-------------------------------
'
'0.0.1-alpha - Arelyel - Initial scaffold code / placeholders
'0.0.2-alpha - Arelyel - More planning / TODOs
'0.0.3-alpha - Arelyel - Standard Newton calculation