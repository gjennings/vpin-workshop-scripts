'***************************************************************
' ZCLK: VPIN WORKSHOP CLOCKS - 0.2.1 ALPHA
'***************************************************************
' WHAT IS IT?
' The VPin Workshop Clocks class is a simple utility class for
' tracking time-based operations in the table with GameTime
' precision. It can also be used to fire a callback when a
' clock expires. And clocks can be paused / unpaused.
'
' This is intended for recurring clocks, such as mode times,
' ball saves, and so on. It is not intended for temporary
' or one-off timers which would be better suited using either
' VPX timers, vpmTimer, or vpwQueueManager (VPW queue system).
'
' HOW TO USE
' 1) Initialize the class on a variable:
'    Dim Clocks : set Clocks = new vpwClocks
' 2) Use a VPX timer to tick the clocks with vpwClocks.Tick.
'    You can use any interval, but the lower the interval, the
'    more precise the clocks will expire.
' 3) Add your clocks via vpwClocks.Add.
' 4) Refer to the vpwClock for how to control your clock.
'    Control your clock by changing its properties via
'    vpwClocks.data.Item(clockName).
'
' TUTORIAL: https://youtu.be/QvYl0P09Uw4
'***************************************************************
Class vpwClocks
	Public data ' Dictionary of clocks we are using
	Public lastTick ' GameTime which Tick was last called
	
	Private Sub Class_Initialize
		lastTick = GameTime
		Set data = CreateObject("Scripting.Dictionary")
	End Sub
	
	'----------------------------------------------------------
	' vpwClocks.Add
	' Register a new clock.
	'
	' PARAMETERS:
	' cName (String) - The unique name for this clock.
	' Caution! If you use the name of a clock that already
	' exists, it will be overwritten!
	'
	' After adding a clock, use vpwClocks.data.Item(cName)
	' to set properties on the clock (see vpwClock class).
	'----------------------------------------------------------
	Public Sub Add(cName)
		' Remove duplicates
		If data.Exists(cName) Then data.Remove cName
		
		' Add our new clock
		Dim i
		Set i = New vpwClock
		data.Add cName, i
	End Sub
	
	Public Sub Tick()
		If data.Count = 0 Then Exit Sub ' Nothing to Do
		
		' Tick every clock
		Dim k, key, item
		k = data.Keys
		For Each key In k
			Set item = data.Item(key)
			item.Tick Int(GameTime - lastTick)
		Next
		
		' Set when we last ticked
		lastTick = GameTime
	End Sub
End Class

Class vpwClock ' An individual clock.
	Public timeLeft ' Integer - The number of milliseconds until the clock expires (0 = currently expired)
	Public isPaused ' Boolean - Whether or not the clock Is paused (timeLeft will not decrease when True)
	Public canExpire ' Boolean - Whether or not the clock can actually expire right now (if false, timeLeft will never go below 1 until this is true)
	
	Private c_expiryCallback
	Private c_tickCallback
	
	Private Sub Class_Initialize
		timeLeft = 0
		isPaused = False
		canExpire = True
		c_expiryCallback = Null
	End Sub
	
	Public Property Get expiryCallback() ' String|Object|Null - The expiry callback for this clock
		If IsObject(c_expiryCallback) Then
			Set expiryCallback = c_expiryCallback
		Else
			expiryCallback = c_expiryCallback
		End If
	End Property
	
	'----------------------------------------------------------
	' Let|Set vpwClock.expiryCallback
	' Assign a callback to be fired when the clock expires.
	'
	' PARAMETERS:
	' callback (String|Object|Null) - Either a string of the
	' routine to fire, an Object of the routine, or Null if
	' no callback should be fired when this clock expires.
	'----------------------------------------------------------
	Public Property Let expiryCallback(callback)
		If IsObject(callback) Then
			Set c_expiryCallback = callback
		Else
			c_expiryCallback = callback
		End If
	End Property
	
	Public Property Set expiryCallback(callback)
		Set c_expiryCallback = callback
	End Property
	
	Public Property Get tickCallback() ' String|Null - The tick callback for this clock
		tickCallback = c_tickCallback
	End Property
	
	'----------------------------------------------------------
	' Let vpwClock.tickCallback
	' Assign a callback to be fired when the clock ticks.
	' NOTE: Will stop firing when clock expires, is paused, or
	' out of time but cannot yet expire. Tick callback fires
	' before expiry callback.
	'
	' PARAMETERS:
	' callback (String|Null) - Either a string of the
	' routine to fire or Null if no callback should be fired
	' when this clock ticks.
	'
	' WARNING! Should be specified as the name of a sub
	' without any parameters in the string; the following
	' parameters are passed automatically into the sub:
	' timeLeft (number) - Time left in milliseconds
	' timeElapsed (number) - Milliseconds that elapsed since
	' the previous tick
	'----------------------------------------------------------
	Public Property Let tickCallback(callback)
		c_tickCallback = callback
	End Property
	
	Public Sub Tick(milli) ' Tick this clock and handle expiration
		' Do not tick if expired or paused
		If timeLeft <= 0 Or isPaused = True Then Exit Sub
		
		' Tick down the specified number of milliseconds
		timeLeft = timeLeft - Int(milli)
		If timeLeft < 0 Then
			milli = Int(milli) + timeLeft
			timeLeft = 0
		End If
		
		' Did the clock expire?
		If timeLeft <= 0 Then
			' The clock can expire, so expire it
			If canExpire = True Then
				timeLeft = 0
				
				'Fire tick callback if applicable
				If VarType(c_tickCallback) = vbString Then ExecuteGlobal c_tickCallback & " " & timeLeft & ", " & milli
				
				' Fire callback if applicable
				If IsObject(c_expiryCallback) Then
					Call c_expiryCallback(0)
				ElseIf VarType(c_expiryCallback) = vbString Then
					ExecuteGlobal c_expiryCallback
				End If
			Else
				timeLeft = 1 ' clock cannot expire yet
			End If
		Else
			'Fire tick callback if applicable
			If VarType(c_tickCallback) = vbString Then ExecuteGlobal c_tickCallback & " " & timeLeft & ", " & milli
		End If
	End Sub
End Class

'**************************************************************
'   END VPWCLOCKS
'**************************************************************