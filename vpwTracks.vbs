'***************************************************************
' ZTRK: VPIN WORKSHOP DYNAMIC SOUND FADER - 0.1.2 ALPHA
'***************************************************************
' WHAT IS IT?
' The VPin Workshop Dynamic Sound Fader is a utility class
' for managing sound played through VPX's sound manager
' library. This class allows for easy management of track
' fading, pitching, and/or panning over a period of time.
'
' HOW TO USE
' 1) Put this VBS file in your VPX Scripts folder, or
'    copy / paste the code into your table script
'    (and skip step 2).
' 2) Include this VBS file in your table via ExecuteGlobal and
'    Scripting.FileSystemObject.
' 3) Initialize the class on a variable:
'    Dim Tracks : set Tracks = new vpwTracks
' 4) Use a VPX timer to tick the fade with vpwTracks.Tick.
'    You can use any interval, but the lower the interval, the
'    more smooth the fading.
' 5) Instead of using VPX's PlaySound or PlayMusic, use
'    With vpwTracks
'        .PlayTrack "name_of_sound", loopCount, initialVolume, initialPan, randomPitch, initialPitch, useExisting, restart, initialFrontRearFade (essentially, same parameters as PlaySound, except all are required)
'        .AddFade "name_of_sound", "volume", targetVolume, fadeTimeMilliseconds (for fading the volume. You can also use pan, pitch, or frontRearFade. Note that if you used randomPitch then pitch fading will NOT work correctly.)
'        (You can add any number of additional AddFades here,
'        or even add more AddFades later so long as the track
'        is still playing. If it's not playing, you should call
'        PlayTrack again first. Note that you cannot overlap
'        multiple fades of the same type [param 2]; they will
'        overwrite each other if the previous did not yet finish)
'    End With
' 6) Refer to the rest of this class for instructions.
'
' Consider also using vpwMusic to manage your music.
'
' Note: For sounds for which you will never use dynamic
' fading, use PlaySound / StopSound instead of vpwTracks.
' Using tracks in vpwTracks which will never be faded will
' increase the performance / resource use of vpwTracks
' for no good reason.
'
' TUTORIAL: https://youtu.be/lAwVL60e2gU
'***************************************************************

Class vpwTracks
	Public data             'A dictionary of VPX sound tracks currently being processed (key is the sound track name in VPX, value is the vpwTrack class)
	Private lastTick        'GameTime of when Tick was last called
	
	Private Sub Class_Initialize
		Set data = CreateObject("Scripting.Dictionary")
		lastTick = GameTime
	End Sub
	
	Private Function GetOrCreateTrack(trackName) 'Find or create a VPX sound track
		If IsNull(trackName) Then Exit Function 'Might be used with vpwMusic nowPlaying, which could be null
		
		If data.Exists(trackName) Then
			Set GetOrCreateTrack = data.Item(trackName)
			Exit Function
		End If
		
		Dim track
		Set track = new vpwTrack
		track.name = trackName
		Set data.Item(trackName) = track
		Set GetOrCreateTrack = data.Item(trackName)
	End Function
	
	'----------------------------------------------------------
	' vpwTracks.PlayTrack
	' Replacement for PlaySound; play a VPX sound.
	'
	' PARAMETERS:
	' trackName (String) - The name of the VPX sound to play.
	' loopCount (Integer) - The number of times to play the
	' sound. Use -1 to play infinitely until stopped.
	' volume (float) - The initial volume to use (0 - 1).
	' Use null to maintain current volume if played before.
	' Pan (float) - ranges from -1.0 (left) over 0.0 (both) to
	' 1.0 (right). Use null to maintain current pan if
	' played before.
	' randomPitch (float) - ranges from 0.0 (no randomization)
	' to 1.0 (vary between half speed to double speed).
	' pitch (integer) - can be positive or negative and directly
	' adds onto the standard sample frequency. Use null for the
	' current pitch if played before.
	' useExisting (Boolean) - Instead of playing the sound on a
	' new channel, use the existing channel if it is currently
	' playing.
	' restart (Boolean) - Start the sound over from the
	' beginning.
	' frontRearFade (float) - similar to pan but fades between
	' the front and rear speakers. Use null to maintain the
	' current fade if played before.
	'
	' WARNING: Use of AddFade may break randomPitch if used.
	'
	' Note that all parameters are required even though that
	' is not the case for PlaySound.
	'----------------------------------------------------------
	Public Sub PlayTrack(trackName, loopCount, volume, pan, randomPitch, pitch, useExisting, restart, frontRearFade)
		If IsNull(trackName) Then Exit Sub 'Might be used with vpwMusic nowPlaying, which could be null
		
		Dim track
		Set track = GetOrCreateTrack(trackName)
		track.PlayTrack loopCount, volume, pan, randomPitch, pitch, useExisting, restart, frontRearFade
	End Sub
	
	'----------------------------------------------------------
	' vpwTracks.AddFade
	' Add a fade operation to a track that is playing.
	' Note that fade operations run in parallel, and specifying
	' the same fadeName before a previous one finishes will
	' overwrite it.
	' If a track is not playing, you should call PlayTrack
	' first or the track might not sound right when it starts.
	' Fading might break randomPitch if it was used!
	'
	' PARAMETERS:
	' trackName (String) - Name of sound in VPX to add the
	' fade operation.
	' fadeName (String) - volume, pan, pitch, or frontRearFade.
	' targetValue (float) - The [fadeName] of the track should
	' gradually change from its current value to the
	' targetValue over the specified duration period.
	' duration (Integer) - Number of milliseconds the fade
	' should take.
	'----------------------------------------------------------
	Public Sub AddFade(trackName, fadeName, targetValue, duration)
		If IsNull(trackName) Then Exit Sub 'Might be used with vpwMusic nowPlaying, which could be null
		
		Dim track
		Set track = GetOrCreateTrack(trackName)
		track.AddFade fadeName, targetValue, duration
	End Sub
	
	'----------------------------------------------------------
	' vpwTracks.StopTrack
	' Immediately stop playing a track.
	'
	' PARAMETERS:
	' trackName (String) - Name of sound in VPX to stop.
	'----------------------------------------------------------
	Public Sub StopTrack(trackName)
		If Not data.Exists(trackName) Then Exit Sub
		data.Item(trackName).StopTrack
	End Sub
	
	'----------------------------------------------------------
	' vpwTracks.Tick
	' Tick each track (which re-processes fading levels).
	'----------------------------------------------------------
	Public Sub Tick()
		' Calculate time since last call
		Dim timeElapsed
		timeElapsed = GameTime - lastTick
		lastTick = GameTime
		
		' Do nothing if there are no processing tracks
		If data.Count <= 0 Then Exit Sub
		
		' Run the Tick operation on each track
		Dim k, key
		k = data.Keys
		For Each key In k
			data.Item(key).Tick timeElapsed
		Next
	End Sub
End Class

Class vpwTrack ' Do not use directly; use vpwTracks instead!
	Public name                     'The name of the sound in the VPX sound manager
	Public tLoopCount               'The number of times to loop
	Public tVolume                  'The current volume of the track (float, 0 - 1)
	Public tPan                     'pan ranges from -1.0 (left) over 0.0 (both) to 1.0 (right)
	Public tPitch                   'can be positive or negative and directly adds onto the standard sample frequency
	Public tFrontRearFade           'similar to pan but fades between the front and rear speakers
	
	Public data 'Dictionary of active fading operations. Key is operation; value is array (initialValue, targetValue, totalTime, timeLeft, stopSoundWhenDone)
	
	Private Sub Class_Initialize ' You MUST set name (String) after initialization!
		name = Null
		tVolume = 0.0
		tLoopCount = 1
		tPan = 0.0
		tPitch = 0.0
		tFrontRearFade = 0.0
		Set data = CreateObject("Scripting.Dictionary")
	End Sub
	
	Public Sub PlayTrack(loopCount, volume, pan, randomPitch, pitch, useExisting, restart, frontRearFade) ' Play the track, making note of its parameters
		' Determine what volume, loopCount, pan, pitch, and frontRearFade to use; and note their values in the class.
		Dim pVolume
		If IsNull(volume) Then
			pVolume = tVolume
		Else
			pVolume = volume
			tVolume = volume
		End If
		Dim pLoopCount
		If IsNull(loopCount) Then
			pLoopCount = tLoopCount
		Else
			pLoopCount = loopCount
			tLoopCount = loopCount
		End If
		Dim pPan
		If IsNull(pan) Then
			pPan = tPan
		Else
			pPan = pan
			tPan = pan
		End If
		Dim pPitch
		If IsNull(pitch) Then
			pPitch = tPitch
		Else
			If pitch = 0 Then pitch = 1 'We treat a pitch of 0 as "use original pitch", however VPX treats 0 as "maintain current pitch"
			pPitch = pitch
			tPitch = pitch
		End If
		
		Dim pFrontRearFade
		If IsNull(frontRearFade) Then
			pFrontRearFade = tFrontRearFade
		Else
			pFrontRearFade = frontRearFade
			tFrontRearFade = frontRearFade
		End If
		
		' Play the sound
		PlaySound name, pLoopCount, pVolume, pPan, randomPitch, pPitch, useExisting, restart, pfrontRearFade
	End Sub
	
	Public Sub AddFade(fadeName, targetValue, duration) ' Add a fade operation to the track
		' Note: When adding a fade operation, we are overwriting any existing operations of the same fade type.
		' We use the current volume/pan/pitch/fade as the initial value so when overwrites happen, the fade will start at the value left off
		' to prevent "jumping".
		Select Case fadeName
			Case "volume":
				data.Item(fadeName) = Array(tVolume, targetValue, 0, duration)
				
			Case "pan":
				data.Item(fadeName) = Array(tPan, targetValue, 0, duration)
				
			Case "pitch":
				data.Item(fadeName) = Array(tPitch, targetValue, 0, duration)
				
			Case "frontRearFade":
				data.Item(fadeName) = Array(tFrontRearFade, targetValue, 0, duration)
		End Select
	End Sub
	
	Public Sub Tick(timeElapsed) ' Process fading on the track
		If data.Count = 0 Then Exit Sub
		
		' loop through each fade operation
		Dim k, key, fadeValue, totalTimeElapsed, timeRemaining, valueDifference, percentProgress, newValue, reTriggerPlaySound
		k = data.Keys
		reTriggerPlaySound = False
		For Each key In k
			' Determine the total time that has elapsed, and the time remaining, for the fade operation
			totalTimeElapsed = data.Item(key)(2) + timeElapsed
			timeRemaining = data.Item(key)(3) - totalTimeElapsed
			If timeRemaining < 0 Then timeRemaining = 0
			If totalTimeElapsed > data.Item(key)(3) Then totalTimeElapsed = data.Item(key)(3)
			
			' Determine the difference between initial and target value, the percent complete from time elapsed to total fade time, and thus what value we should be using for the fade right now.
			valueDifference = Abs(data.Item(key)(0) - data.Item(key)(1))
			If data.Item(key)(3) <= 0 Then ' Division by zero / negative duration protection
				percentProgress = 1
			Else
				percentProgress = totalTimeElapsed / data.Item(key)(3)
			End If
			If data.Item(key)(0) > data.Item(key)(1) Then ' Decreasing the value over time
				newValue = data.Item(key)(0) - (valueDifference * percentProgress)
			Else ' Increasing the value over time
				newValue = data.Item(key)(0) + (valueDifference * percentProgress)
			End If
			
			' Save our new parameters
			data.Item(key) = Array(data.Item(key)(0), data.Item(key)(1), totalTimeElapsed, data.Item(key)(3))
			
			' Update the track's audio values in the class, and mark that we must call PlaySound
			Select Case key
				Case "volume":
					tVolume = newValue
					reTriggerPlaySound = True
					
				Case "pan":
					tPan = newValue
					reTriggerPlaySound = True
					
				Case "pitch":
					tPitch = newValue
					If tPitch = 0 Then tPitch = 1 'We treat a pitch of 0 as "use original pitch", however VPX treats 0 as "maintain current pitch"
					reTriggerPlaySound = True
					
				Case "frontRearFade":
					tFrontRearFade = newValue
					reTriggerPlaySound = True
			End Select
			
			'Debug.print name & ": " & key & ": " & timeRemaining & " / " & valueDifference & " / " & percentProgress & " / " & newValue
			
			' Remove the fade operation if it is done
			If timeRemaining <= 0 Then data.Remove key
		Next
		
		' Call PlaySound with the new values, re-using the current channel without restarting it
		If reTriggerPlaySound = True Then PlaySound name, tLoopCount, tVolume, tPan, 0, tPitch, True, False, tFrontRearFade
		
		' We want To actually stop playing the track if the volume is 0 and there are no more fade operations
		If tVolume <= 0 And data.Count <= 0 Then
			StopSound name
		End If
	End Sub
	
	Public Sub StopTrack() ' Immediately stop playing the track, clear out all fade operations, and set volume To 0
		data.RemoveAll
		StopSound name
		tVolume = 0.0
	End Sub
End Class

'**************************************************************
'   END VPWTRACKS
'**************************************************************